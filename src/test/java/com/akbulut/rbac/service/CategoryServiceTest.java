/*
package com.akbulut.rback.service;

import com.akbulut.rback.entity.Category;
import com.akbulut.rback.entity.User;
import com.akbulut.rback.repository.CategoryRepository;
import com.akbulut.rback.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {
    @Mock
    private CategoryRepository repository;

    @InjectMocks
    private CategoryService service;

    @Test
    public void findAll() {
        Category newCategory = Category.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .build();
        List<Category> expected = Arrays.asList(newCategory);

        when(repository.findAll()).thenReturn(expected);

        List<Category> actualList = service.findAll();

        assertThat(actualList).isEqualTo(expected);

        service.deleteById(newCategory.getId());
    }

    @Test
    public void findById() {
        Category newCategory = Category.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .build();

        when(repository.findById(newCategory.getId())).thenReturn(java.util.Optional.of(newCategory));

        Category actual = service.findById(newCategory.getId()).get();

        assertThat(actual).isEqualTo(newCategory);

        service.deleteById(newCategory.getId());
    }

    @Test
    public void save() {
        Category newCategory = Category.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .build();
        when(repository.save(newCategory)).thenReturn(newCategory);

        Category created = service.save(newCategory);

        assertThat(created).isNotNull();
        assertThat(created).isEqualTo(newCategory);

        service.deleteById(newCategory.getId());
    }

    @Test
    public void deleteById() {
        Category newCategory = Category.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .build();
        repository.save(newCategory);

        repository.deleteById(newCategory.getId());

        verify(repository, times(1)).deleteById(newCategory.getId());
    }
}
*/
