/*
package com.akbulut.rback.service;

import com.akbulut.rback.entity.User;
import com.akbulut.rback.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository repository;

    @InjectMocks
    private UserService service;

    @Test
    public void findAll() {
        User newUser = User.builder()
                .id(1234L)
                .username("testName")
                .password("testPassword")
                .build();
        List<User> expected = Arrays.asList(newUser);

        when(repository.findAll()).thenReturn(expected);

        List<User> actualList = service.findAll();

        assertThat(actualList).isEqualTo(expected);

        service.deleteById(newUser.getId());
    }

    @Test
    public void findById() {
        User newUser = User.builder()
                .id(1234L)
                .username("testName")
                .password("testPassword")
                .build();

        when(repository.findById(newUser.getId())).thenReturn(java.util.Optional.of(newUser));

        User actual = service.findById(newUser.getId()).get();

        assertThat(actual).isEqualTo(newUser);

        service.deleteById(newUser.getId());
    }

    @Test
    public void save() {
        User newUser = User.builder()
                .id(1234L)
                .username("testName")
                .password("testPassword")
                .build();
        when(repository.save(newUser)).thenReturn(newUser);

        User created = service.save(newUser);

        assertThat(created).isNotNull();
        assertThat(created).isEqualTo(newUser);

        service.deleteById(newUser.getId());
    }

    @Test
    public void deleteById() {
        User newUser = User.builder()
                .id(1234L)
                .username("testName")
                .password("testPassword")
                .build();
        repository.save(newUser);

        repository.deleteById(newUser.getId());

        verify(repository, times(1)).deleteById(newUser.getId());
    }
}
*/
