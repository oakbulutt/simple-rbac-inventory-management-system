/*
package com.akbulut.rback.service;

import com.akbulut.rback.entity.Product;
import com.akbulut.rback.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
    @Mock
    private ProductRepository repository;

    @InjectMocks
    private ProductService service;

    @Test
    public void findAll() {
        Product newProduct = Product.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .price(new BigDecimal("55"))
                .build();
        List<Product> expected = Arrays.asList(newProduct);

        when(repository.findAll()).thenReturn(expected);

        List<Product> actualList = service.findAll();

        assertThat(actualList).isEqualTo(expected);

        service.deleteById(newProduct.getId());
    }

    @Test
    public void findById() {
        Product newProduct = Product.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .price(new BigDecimal("55"))
                .build();

        when(repository.findById(newProduct.getId())).thenReturn(java.util.Optional.of(newProduct));

        Product actual = service.findById(newProduct.getId()).get();

        assertThat(actual).isEqualTo(newProduct);

        service.deleteById(newProduct.getId());
    }

    @Test
    public void save() {
        Product newProduct = Product.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .price(new BigDecimal("55"))
                .build();
        when(repository.save(newProduct)).thenReturn(newProduct);

        Product created = service.save(newProduct);

        assertThat(created).isNotNull();
        assertThat(created).isEqualTo(newProduct);

        service.deleteById(newProduct.getId());
    }

    @Test
    public void deleteById() {
        Product newProduct = Product.builder()
                .id(1234L)
                .name("testName")
                .description("testDescription")
                .price(new BigDecimal("55"))
                .build();
        repository.save(newProduct);

        repository.deleteById(newProduct.getId());

        verify(repository, times(1)).deleteById(newProduct.getId());
    }
}
*/
