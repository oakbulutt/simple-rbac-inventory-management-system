/*
package com.akbulut.rback.controller;

import com.akbulut.rback.entity.Product;
import com.akbulut.rback.mapper.ProductMapper;
import com.akbulut.rback.service.ProductService;
import com.akbulut.rback.service.dto.ProductDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProductIT {
    private static final String PRODUCTS_ENDPOINT_URL = "/products";
    private static final String PRODUCT_ENDPOINT_URL = "/products/{product-id}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductMapper productMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void findAll() throws Exception {
        ResultActions resultActions = mockMvc.perform(get(PRODUCTS_ENDPOINT_URL)
                .contentType(MediaType.APPLICATION_JSON));
        String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        List<ProductDTO> products = objectMapper.readValue(contentAsString, List.class);

        assertThat(products).isNotNull();
        assertThat(products).extracting("id", "name")
                .containsExactlyInAnyOrder(
                        tuple(1234, "testName"));
    }

    @Test
    void create() {
    }

    @Test
    void findById() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}*/
