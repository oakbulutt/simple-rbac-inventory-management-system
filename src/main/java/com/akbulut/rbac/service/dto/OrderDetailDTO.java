package com.akbulut.rbac.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailDTO {
    private Long id;
    private int quantity;
    private OrderDTO orderDTO;
    private CategoryDTO categoryDTO;
}
