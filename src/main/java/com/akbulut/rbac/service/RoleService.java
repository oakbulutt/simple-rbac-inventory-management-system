package com.akbulut.rbac.service;


import com.akbulut.rbac.entity.Role;
import com.akbulut.rbac.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Optional<Role> findById(Long id) {
        return roleRepository.findById(id);
    }

    public Role save(Role user) {
        return roleRepository.save(user);
    }

    public void deleteById(Long id) {
        roleRepository.deleteById(id);
    }
}
