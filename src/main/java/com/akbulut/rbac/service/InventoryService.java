package com.akbulut.rbac.service;


import com.akbulut.rbac.entity.Inventory;
import com.akbulut.rbac.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InventoryService {

    private final InventoryRepository inventoryRepository;

    public List<Inventory> findAll() {
        return inventoryRepository.findAll();
    }

    public Optional<Inventory> findById(Long id) {
        return inventoryRepository.findById(id);
    }

    public Inventory save(Inventory user) {
        return inventoryRepository.save(user);
    }

    public void deleteById(Long id) {
        inventoryRepository.deleteById(id);
    }
}
