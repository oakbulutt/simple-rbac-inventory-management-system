package com.akbulut.rbac.service;


import com.akbulut.rbac.entity.OrderDetail;
import com.akbulut.rbac.repository.OrderDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderDetailService {

    private final OrderDetailRepository orderDetailRepository;

    public List<OrderDetail> findAll() {
        return orderDetailRepository.findAll();
    }

    public List<OrderDetail> findByOrderId(Long orderId) {
        return orderDetailRepository.findByOrderId(orderId);
    }

    public Optional<OrderDetail> findByIdAndOrderId(Long id, Long orderId) {
        return orderDetailRepository.findByIdAndOrderId(id, orderId);
    }

    public Optional<OrderDetail> findById(Long id) {
        return orderDetailRepository.findById(id);
    }

    public OrderDetail save(OrderDetail user) {
        return orderDetailRepository.save(user);
    }

    public void deleteById(Long id) {
        orderDetailRepository.deleteById(id);
    }
}
