package com.akbulut.rbac.service;


import com.akbulut.rbac.entity.Supplier;
import com.akbulut.rbac.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupplierService {

    private final SupplierRepository supplierRepository;

    public List<Supplier> findAll() {
        return supplierRepository.findAll();
    }

    public Optional<Supplier> findById(Long id) {
        return supplierRepository.findById(id);
    }

    public Supplier save(Supplier user) {
        return supplierRepository.save(user);
    }

    public void deleteById(Long id) {
        supplierRepository.deleteById(id);
    }
}