package com.akbulut.rbac.service;


import com.akbulut.rbac.entity.Customer;
import com.akbulut.rbac.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public Optional<Customer> findById(Long id) {
        return customerRepository.findById(id);
    }

    public Customer save(Customer user) {
        return customerRepository.save(user);
    }

    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }
}

