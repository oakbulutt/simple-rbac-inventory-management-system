package com.akbulut.rbac.service;


import com.akbulut.rbac.entity.Order;
import com.akbulut.rbac.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    public Order save(Order user) {
        return orderRepository.save(user);
    }

    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }
}
