package com.akbulut.rbac.repository;

import com.akbulut.rbac.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
