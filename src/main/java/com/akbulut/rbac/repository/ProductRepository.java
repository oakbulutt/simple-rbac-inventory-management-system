package com.akbulut.rbac.repository;

import com.akbulut.rbac.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByCategoryId(Long categoryId);

    Optional<Product> findByIdAndCategoryId(Long id, Long categoryId);
}
