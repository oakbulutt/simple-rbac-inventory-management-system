package com.akbulut.rbac.repository;

import com.akbulut.rbac.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    List<OrderDetail> findByOrderId(Long orderId);

    Optional<OrderDetail> findByIdAndOrderId(Long id, Long orderId);
}
