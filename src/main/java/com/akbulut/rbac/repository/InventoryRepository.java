package com.akbulut.rbac.repository;

import com.akbulut.rbac.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {
}
