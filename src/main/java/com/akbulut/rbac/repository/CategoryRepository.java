package com.akbulut.rbac.repository;

import com.akbulut.rbac.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
