package com.akbulut.rbac.repository;

import com.akbulut.rbac.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
