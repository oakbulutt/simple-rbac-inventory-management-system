package com.akbulut.rbac.controller;

import com.akbulut.rbac.entity.Role;
import com.akbulut.rbac.mapper.RoleMapper;
import com.akbulut.rbac.service.RoleService;
import com.akbulut.rbac.service.dto.RoleDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequestMapping("/roles")
@RestController
@RequiredArgsConstructor
public class RoleAPI {
    private final RoleService roleService;
    private final RoleMapper roleMapper;

    @GetMapping
    public ResponseEntity<List<RoleDTO>> findAll() {
        return ResponseEntity.ok(roleMapper.toRoleDTOs(roleService.findAll()));
    }

    @PostMapping
    public ResponseEntity<RoleDTO> create(@Valid @RequestBody RoleDTO roleDTO) {
        roleService.save(roleMapper.toRole(roleDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(roleDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RoleDTO> findById(@PathVariable Long id) {
        Optional<Role> role = roleService.findById(id);
        if (!role.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(roleMapper.toRoleDTO(role.get()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<RoleDTO> update(@PathVariable Long id, @Valid @RequestBody RoleDTO roleDTO) {
        if (!roleService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        Role role = roleMapper.toRole(roleDTO);
        role.setId(id);

        roleService.save(role);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(roleDTO);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!roleService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        roleService.deleteById(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
