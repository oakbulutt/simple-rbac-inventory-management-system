package com.akbulut.rbac.controller;

import com.akbulut.rbac.entity.Category;
import com.akbulut.rbac.entity.Product;
import com.akbulut.rbac.mapper.ProductMapper;
import com.akbulut.rbac.service.CategoryService;
import com.akbulut.rbac.service.ProductService;
import com.akbulut.rbac.service.dto.ProductDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProductAPI {
    private final ProductService productService;
    private final ProductMapper productMapper;
    private final CategoryService categoryService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> findAll() {
        List<Product> products = productService.findAll();
        return ResponseEntity.ok(products);
    }

    @RequestMapping(value = "/categories/{category-id}/products", method = RequestMethod.GET)
    public ResponseEntity<List<ProductDTO>> findAllByCategoryId(@PathVariable(value = "category-id") Long categoryId) {
        return ResponseEntity.ok(productMapper.toProductDTOs(productService.findByCategoryId(categoryId)));
    }

    @RequestMapping(value = "/categories/{category-id}/products/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductDTO> findByIdAndCategoryId(@PathVariable(value = "category-id") Long categoryId,
                                                            @PathVariable(value = "id") Long id) {
        Optional<Product> product = productService.findByIdAndCategoryId(id, categoryId);
        if (!product.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(productMapper.toProductDTO(product.get()));
    }

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public ResponseEntity<Product> create(@Valid @RequestBody ProductDTO productDTO) {
        productService.save(productMapper.toProduct(productDTO));

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductDTO> findById(@PathVariable Long id) {
        Optional<Product> product = productService.findById(id);
        if (!product.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(productMapper.toProductDTO(product.get()));

    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProductDTO> update(@PathVariable Long id, @Valid @RequestBody ProductDTO productDTO) {
        if (!productService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        Product toUpdate = productMapper.toProduct(productDTO);
        toUpdate.setId(id);

        productService.save(toUpdate);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(productDTO);
    }

    @RequestMapping(value = "/categories/{category-id}/products/{id}", method = RequestMethod.POST)
    public ResponseEntity<ProductDTO> addCategoryToProduct(@PathVariable(value = "category-id") Long categoryId,
                                                           @PathVariable(value = "id") Long id) {

        Optional<Product> product = productService.findById(id);

        if (!product.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Optional<Category> category = categoryService.findById(categoryId);
        if (!category.isPresent()) {
            log.error("Category " + categoryId + " is not existed");
            ResponseEntity.badRequest().build();
        }

        Product toSave = product.get();

        toSave.setCategory(category.get());

        return ResponseEntity.ok(productMapper.toProductDTO(productService.save(toSave)));

    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable Long id) {
        if (!productService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        productService.deleteById(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
