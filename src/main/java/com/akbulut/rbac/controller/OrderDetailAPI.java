package com.akbulut.rbac.controller;

import com.akbulut.rbac.entity.Order;
import com.akbulut.rbac.entity.OrderDetail;
import com.akbulut.rbac.mapper.OrderDetailMapper;
import com.akbulut.rbac.service.OrderDetailService;
import com.akbulut.rbac.service.OrderService;
import com.akbulut.rbac.service.dto.OrderDetailDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrderDetailAPI {
    private final OrderDetailService orderDetailService;
    private final OrderDetailMapper orderDetailMapper;
    private final OrderService orderService;

    @RequestMapping(value = "/order-details", method = RequestMethod.GET)
    public ResponseEntity<List<OrderDetailDTO>> findAll() {
        return ResponseEntity.ok(orderDetailMapper.toOrderDetailDTOs(orderDetailService.findAll()));
    }

    @RequestMapping(value = "/orders/{order-id}/order-details", method = RequestMethod.GET)
    public ResponseEntity<List<OrderDetailDTO>> findAllByCategoryId(@PathVariable(value = "order-id") Long orderId) {
        return ResponseEntity.ok(orderDetailMapper.toOrderDetailDTOs(orderDetailService.findByOrderId(orderId)));
    }

    @RequestMapping(value = "/orders/{order-id}/order-details/{id}", method = RequestMethod.GET)
    public ResponseEntity<OrderDetailDTO> findByIdAndOrderId(@PathVariable(value = "order-id") Long orderId,
                                                             @PathVariable(value = "id") Long id) {
        Optional<OrderDetail> orderDetail = orderDetailService.findByIdAndOrderId(id, orderId);
        if (!orderDetail.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(orderDetailMapper.toOrderDetailDTO(orderDetail.get()));
    }

    @RequestMapping(value = "/order-details", method = RequestMethod.POST)
    public ResponseEntity<OrderDetailDTO> create(@Valid @RequestBody OrderDetailDTO orderDetailDTO) {
        orderDetailService.save(orderDetailMapper.toOrderDetail(orderDetailDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(orderDetailDTO);
    }

    @RequestMapping(value = "/order-details/{id}", method = RequestMethod.GET)
    public ResponseEntity<OrderDetailDTO> findById(@PathVariable Long id) {
        Optional<OrderDetail> orderDetail = orderDetailService.findById(id);
        if (!orderDetail.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(orderDetailMapper.toOrderDetailDTO(orderDetail.get()));

    }

    @RequestMapping(value = "/order-details/{id}", method = RequestMethod.PUT)
    public ResponseEntity<OrderDetailDTO> update(@PathVariable Long id, @Valid @RequestBody OrderDetailDTO orderDetailDTO) {
        if (!orderDetailService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        OrderDetail toUpdate = orderDetailMapper.toOrderDetail(orderDetailDTO);
        toUpdate.setId(id);

        orderDetailService.save(toUpdate);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(orderDetailDTO);
    }

    @RequestMapping(value = "/orders/{order-id}/order-details/{id}", method = RequestMethod.POST)
    public ResponseEntity<OrderDetail> addOrderDetailToOrder(@PathVariable(value = "order-id") Long orderId,
                                                             @PathVariable(value = "id") Long id) {

        Optional<OrderDetail> orderDetail = orderDetailService.findById(id);

        if (!orderDetail.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Optional<Order> order = orderService.findById(orderId);
        if (!order.isPresent()) {
            log.error("Order " + orderId + " is not existed");
            ResponseEntity.badRequest().build();
        }

        OrderDetail toSave = orderDetail.get();

        toSave.setOrder(order.get());

        return ResponseEntity.ok(orderDetailService.save(toSave));

    }


    @RequestMapping(value = "/order-details/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable Long id) {
        if (!orderDetailService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        orderDetailService.deleteById(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
