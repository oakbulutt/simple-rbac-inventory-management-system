package com.akbulut.rbac.controller;

import com.akbulut.rbac.entity.Inventory;
import com.akbulut.rbac.mapper.InventoryMapper;
import com.akbulut.rbac.service.InventoryService;
import com.akbulut.rbac.service.dto.InventoryDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequestMapping("/inventories")
@RestController
@RequiredArgsConstructor
public class InventoryAPI {
    private final InventoryService inventoryService;
    private final InventoryMapper inventoryMapper;

    @GetMapping
    public ResponseEntity<List<InventoryDTO>> findAll() {
        return ResponseEntity.ok(inventoryMapper.toInventoryDTOs(inventoryService.findAll()));
    }

    @PostMapping
    public ResponseEntity<InventoryDTO> create(@Valid @RequestBody InventoryDTO inventoryDTO) {
        inventoryService.save(inventoryMapper.toInventory(inventoryDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(inventoryDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<InventoryDTO> findById(@PathVariable Long id) {
        Optional<Inventory> inventory = inventoryService.findById(id);
        if (!inventory.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(inventoryMapper.toInventoryDTO(inventory.get()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<InventoryDTO> update(@PathVariable Long id, @Valid @RequestBody InventoryDTO inventoryDTO) {
        if (!inventoryService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        Inventory inventory = inventoryMapper.toInventory(inventoryDTO);
        inventory.setId(id);

        inventoryService.save(inventory);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(inventoryDTO);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!inventoryService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        inventoryService.deleteById(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
