package com.akbulut.rbac.controller;

import com.akbulut.rbac.entity.Supplier;
import com.akbulut.rbac.mapper.SupplierMapper;
import com.akbulut.rbac.service.SupplierService;
import com.akbulut.rbac.service.dto.SupplierDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequestMapping("/suppliers")
@RestController
@RequiredArgsConstructor
public class SupplierAPI {
    private final SupplierService supplierService;
    private final SupplierMapper supplierMapper;

    @GetMapping
    public ResponseEntity<List<SupplierDTO>> findAll() {
        return ResponseEntity.ok(supplierMapper.toSupplierDTOs(supplierService.findAll()));
    }

    @PostMapping
    public ResponseEntity<SupplierDTO> create(@Valid @RequestBody SupplierDTO supplierDTO) {
        supplierService.save(supplierMapper.toSupplier(supplierDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(supplierDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupplierDTO> findById(@PathVariable Long id) {
        Optional<Supplier> supplier = supplierService.findById(id);
        if (!supplier.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(supplierMapper.toSupplierDTO(supplier.get()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SupplierDTO> update(@PathVariable Long id, @Valid @RequestBody SupplierDTO supplierDTO) {
        if (!supplierService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        Supplier supplier = supplierMapper.toSupplier(supplierDTO);
        supplier.setId(id);

        supplierService.save(supplier);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(supplierDTO);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!supplierService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        supplierService.deleteById(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
