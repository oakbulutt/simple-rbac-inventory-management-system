package com.akbulut.rbac.controller;

import com.akbulut.rbac.entity.Role;
import com.akbulut.rbac.entity.User;
import com.akbulut.rbac.mapper.UserMapper;
import com.akbulut.rbac.service.RoleService;
import com.akbulut.rbac.service.UserService;
import com.akbulut.rbac.service.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserAPI {
    private final UserService userService;
    private final UserMapper userMapper;
    private final RoleService roleService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<User>> findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> findById(@PathVariable Long id) {
        Optional<User> user = userService.findById(id);
        if (!user.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(userMapper.toUserDTO(user.get()));
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> create(@Valid @RequestBody UserDTO userDTO) {
        userService.save(userMapper.toUser(userDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserDTO> update(@PathVariable Long id, @Valid @RequestBody UserDTO userDTO) {
        if (!userService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        User user = userMapper.toUser(userDTO);
        user.setId(id);

        userService.save(user);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userDTO);
    }

    @RequestMapping(value = "/roles/{role-id}/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserDTO> addRoleToUser(@PathVariable(value = "role-id") Long roleId,
                                                 @PathVariable(value = "id") Long id) {

        Optional<User> user = userService.findById(id);

        if (!user.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Optional<Role> role = roleService.findById(roleId);
        if (!role.isPresent()) {
            log.error("Role " + roleId + " is not existed");
            ResponseEntity.badRequest().build();
        }

        User toSave = user.get();

        toSave.getRoles().add(role.get());

        return ResponseEntity.ok(userMapper.toUserDTO(userService.save(toSave)));

    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable Long id) {
        if (!userService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        userService.deleteById(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
