package com.akbulut.rbac.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new CustomUserDetailsServiceImpl();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers("/")
                .hasAnyAuthority("SystemAdmin", "InventoryManager", "SalesAgent",
                        "InventoryCounter", "ShippingAgent", "PickerPacker")

                // Users
                .antMatchers(HttpMethod.GET, "/users").hasAnyAuthority("SystemAdmin", "InventoryManager")
                .antMatchers(HttpMethod.PUT, "/users").hasAuthority("SystemAdmin")
                .antMatchers(HttpMethod.POST, "/users").hasAuthority("SystemAdmin")
                .antMatchers(HttpMethod.DELETE, "/users").hasAuthority("SystemAdmin")

                // Orders & OrderDetails
                .antMatchers(HttpMethod.GET, "/orders").hasAnyAuthority("SystemAdmin", "InventoryManager", "SalesAgent", "ShippingAgent", "PickerPacker")
                .antMatchers(HttpMethod.GET, "/order-details").hasAnyAuthority("SystemAdmin", "InventoryManager", "SalesAgent", "ShippingAgent", "PickerPacker")
                .antMatchers(HttpMethod.POST, "/orders").hasAnyAuthority("InventoryManager", "SalesAgent")
                .antMatchers(HttpMethod.POST, "/order-details").hasAnyAuthority("InventoryManager", "SalesAgent")
                .antMatchers(HttpMethod.PUT, "/orders").hasAnyAuthority("InventoryManager", "SalesAgent", "ShippingAgent")
                .antMatchers(HttpMethod.PUT, "/order-details").hasAnyAuthority("InventoryManager", "SalesAgent", "ShippingAgent")
                .antMatchers(HttpMethod.DELETE, "/orders").hasAuthority("SystemAdmin")
                .antMatchers(HttpMethod.DELETE, "/order-details").hasAuthority("SystemAdmin")

                // Customers
                .antMatchers(HttpMethod.GET, "/customers").hasAnyAuthority("SystemAdmin", "SalesAgent", "ShippingAgent")
                .antMatchers(HttpMethod.PUT, "/customers").hasAnyAuthority("SalesAgent")
                .antMatchers(HttpMethod.POST, "/customers").hasAnyAuthority("SalesAgent")
                .antMatchers(HttpMethod.DELETE, "/customers").hasAuthority("SystemAdmin")

                // Products & Categories & Inventory
                .antMatchers(HttpMethod.GET, "/products")
                .hasAnyAuthority("SystemAdmin", "InventoryManager", "SalesAgent", "InventoryCounter", "ShippingAgent", "PickerPacker")
                .antMatchers(HttpMethod.GET, "/categories").hasAnyAuthority("SystemAdmin", "InventoryManager", "SalesAgent", "InventoryCounter", "ShippingAgent", "PickerPacker")
                .antMatchers(HttpMethod.GET, "/inventories").hasAnyAuthority("SystemAdmin", "InventoryManager", "SalesAgent", "InventoryCounter", "ShippingAgent", "PickerPacker")
                .antMatchers(HttpMethod.PUT, "/products").hasAnyAuthority("InventoryManager", "InventoryCounter")
                .antMatchers(HttpMethod.PUT, "/categories").hasAnyAuthority("InventoryManager", "InventoryCounter")
                .antMatchers(HttpMethod.PUT, "/inventories").hasAnyAuthority("InventoryManager", "InventoryCounter")
                .antMatchers(HttpMethod.POST, "/products").hasAnyAuthority("InventoryManager")
                .antMatchers(HttpMethod.POST, "/inventories").hasAnyAuthority("InventoryManager")
                .antMatchers(HttpMethod.POST, "/categories").hasAnyAuthority("InventoryManager")
                .antMatchers(HttpMethod.DELETE, "/products").hasAuthority("SystemAdmin")
                .antMatchers(HttpMethod.DELETE, "/inventories").hasAuthority("SystemAdmin")
                .antMatchers(HttpMethod.DELETE, "/categories").hasAuthority("SystemAdmin")

                // Suppliers
                .antMatchers(HttpMethod.GET, "/suppliers").hasAnyAuthority("SystemAdmin", "InventoryManager", "ShippingAgent")
                .antMatchers(HttpMethod.PUT, "/suppliers").hasAnyAuthority("InventoryManager")
                .antMatchers(HttpMethod.POST, "/suppliers").hasAnyAuthority("InventoryManager")
                .antMatchers(HttpMethod.DELETE, "/suppliers").hasAuthority("SystemAdmin")

                .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .formLogin().disable();
    }
}