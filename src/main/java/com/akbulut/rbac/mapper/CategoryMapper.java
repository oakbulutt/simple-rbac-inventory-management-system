package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.Category;
import com.akbulut.rbac.service.dto.CategoryDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CategoryMapper {
    CategoryDTO toCategoryDTO(Category category);

    List<CategoryDTO> toCategoryDTOs(List<Category> category);

    Category toCategory(CategoryDTO categoryDTO);

    List<Category> toCategories(List<CategoryDTO> categoryDTOs);

}
