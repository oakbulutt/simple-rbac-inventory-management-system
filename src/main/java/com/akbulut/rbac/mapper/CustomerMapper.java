package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.Customer;
import com.akbulut.rbac.service.dto.CustomerDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CustomerMapper {
    CustomerDTO toCustomerDTO(Customer customer);

    List<CustomerDTO> toCustomerDTOs(List<Customer> customers);

    Customer toCustomer(CustomerDTO customerDTO);

    List<Customer> toCustomers(List<CustomerDTO> customerDTOs);
}
