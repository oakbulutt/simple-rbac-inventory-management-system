package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.Inventory;
import com.akbulut.rbac.service.dto.InventoryDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface InventoryMapper {
    InventoryDTO toInventoryDTO(Inventory inventory);

    List<InventoryDTO> toInventoryDTOs(List<Inventory> inventories);

    Inventory toInventory(InventoryDTO inventoryDTO);

    List<Inventory> toInventories(List<InventoryDTO> inventoryDTOs);
}
