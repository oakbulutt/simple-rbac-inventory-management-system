package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.Order;
import com.akbulut.rbac.service.dto.OrderDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {
    OrderDTO toOrderDTO(Order order);

    List<OrderDTO> toOrderDTOs(List<Order> orders);

    Order toOrder(OrderDTO OrderDTO);

    List<Order> toOrders(List<OrderDTO> OrderDTOs);
}
