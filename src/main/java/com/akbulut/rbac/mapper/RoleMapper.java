package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.Role;
import com.akbulut.rbac.service.dto.RoleDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {
    RoleDTO toRoleDTO(Role role);

    List<RoleDTO> toRoleDTOs(List<Role> roles);

    Role toRole(RoleDTO roleDTO);

    List<Role> toRoles(List<RoleDTO> roleDTOS);
}
