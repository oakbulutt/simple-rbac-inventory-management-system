package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.OrderDetail;
import com.akbulut.rbac.service.dto.OrderDetailDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface OrderDetailMapper {
    OrderDetailDTO toOrderDetailDTO(OrderDetail orderDetail);

    List<OrderDetailDTO> toOrderDetailDTOs(List<OrderDetail> orders);

    OrderDetail toOrderDetail(OrderDetailDTO orderDTO);

    List<OrderDetail> toOrderDetails(List<OrderDetailDTO> orderDTOs);
}
