package com.akbulut.rbac.mapper;

import com.akbulut.rbac.entity.Supplier;
import com.akbulut.rbac.service.dto.SupplierDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface SupplierMapper {
    SupplierDTO toSupplierDTO(Supplier supplier);

    List<SupplierDTO> toSupplierDTOs(List<Supplier> supplier);

    Supplier toSupplier(SupplierDTO supplierDTO);

    List<Supplier> toSuppliers(List<Supplier> supplierDTOs);
}
